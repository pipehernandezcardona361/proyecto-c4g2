package com.andreshernandez.ventasdomicilio.presenter;


import com.andreshernandez.ventasdomicilio.model.PaymentsInteractor;
import com.andreshernandez.ventasdomicilio.mvp.PaymentsMVP;

import java.util.List;

public class PaymentsPresenter implements PaymentsMVP.Presenter {

    private PaymentsMVP.View view;
    private PaymentsMVP.Model model;

    public PaymentsPresenter(PaymentsMVP.View view) {
        this.view = view;
        this.model = new PaymentsInteractor();
    }

    @Override
    public void loadPayments() {
        view.showProgressBar();
        new Thread(() -> {
            model.loadPayments(new PaymentsMVP.Model.LoadPaymentsCallback() {
                @Override
                public void setPayments(List<PaymentsMVP.PaymentDto> payments) {
                    view.getActivity().runOnUiThread(() -> {
                        view.hideProgressBar();
                        view.showPayments(payments);
                    });
                }
            });
        }).start();
    }

    @Override
    public void onPaymentsClick() {

    }

    @Override
    public void onNewSaleClick() {

    }
}

