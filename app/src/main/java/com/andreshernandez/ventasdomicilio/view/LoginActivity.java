package com.andreshernandez.ventasdomicilio.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.andreshernandez.ventasdomicilio.R;
import com.andreshernandez.ventasdomicilio.mvp.LoginMVP;
import com.andreshernandez.ventasdomicilio.presenter.LoginPresenter;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;



public class LoginActivity extends AppCompatActivity implements LoginMVP.View {

    private LinearProgressIndicator pbWait;
    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;
    private TextInputLayout tilPassword;
    private TextInputEditText etPassword;
    private AppCompatButton btnLogin;
    private AppCompatButton btnFacebook;
    private AppCompatButton btnGoogle;

    private LoginMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        presenter = new LoginPresenter(LoginActivity.this);

        initUI();
    }

    private void initUI() {
        pbWait = findViewById(R.id.pb_wait);

        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(v -> presenter.onLoginClick());

        btnFacebook = findViewById(R.id.btn_facebook);
        btnFacebook.setOnClickListener(v -> presenter.onFacebookClick());

        btnGoogle = findViewById(R.id.btn_google);
        btnGoogle.setOnClickListener(v -> presenter.onGoogleClick());
    }

    @Override
    public Activity getActivity() {
        return LoginActivity.this;
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(
                etEmail.getText().toString().trim(),
                etPassword.getText().toString().trim());
    }

    @Override
    public void showEmailError(String error) {
        tilEmail.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        tilPassword.setError(error);
    }

    @Override
    public void showPaymentsActivity() {
        Intent intent = new Intent(LoginActivity.this, PaymentsActivity.class);
        startActivity(intent);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgresBar() {
        pbWait.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(false);
        btnFacebook.setEnabled(false);
        btnGoogle.setEnabled(false);
    }

    @Override
    public void hideProgresBar() {
        pbWait.setVisibility(View.GONE);
        btnLogin.setEnabled(true);
        btnFacebook.setEnabled(true);
        btnGoogle.setEnabled(true);
    }
}
