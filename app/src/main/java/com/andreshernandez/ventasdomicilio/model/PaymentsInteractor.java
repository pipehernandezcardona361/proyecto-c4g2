package com.andreshernandez.ventasdomicilio.model;


import com.andreshernandez.ventasdomicilio.mvp.PaymentsMVP;

import java.util.Arrays;
import java.util.List;


public class PaymentsInteractor implements PaymentsMVP.Model {

    private List<PaymentsMVP.PaymentDto> payments;

    public PaymentsInteractor(){
        payments = Arrays.asList(
                new PaymentsMVP.PaymentDto("Cesar Diaz", "Calle 1 # 7 - 53"),
                new PaymentsMVP.PaymentDto("Andres Hernandez", "Calle 2 # 15 - 23"),
                new PaymentsMVP.PaymentDto("Estiven Gutierrez", "Calle 3 # 2 - 55"),
                new PaymentsMVP.PaymentDto("Maria Grisales", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentDto("Daniel Gutierrez", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Juan Hernandez", "Cra 21 # 3 - 33"),
                new PaymentsMVP.PaymentDto("Luffy D' Monkey", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentDto("Roronoa Zoro", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Vismoke Sanji", "Cra 21 # 3 - 33"),
                new PaymentsMVP.PaymentDto("Brook", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentDto("Jimbe", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Chopper", "Cra 21 # 3 - 33"),
                new PaymentsMVP.PaymentDto("Nami", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentDto("Franky", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Nico Robin", "Cra 21 # 3 - 33"),
                new PaymentsMVP.PaymentDto("Usoop", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentDto("Drakule Milwahk", "Urb. El Corral Mz A CS 1"),
                new PaymentsMVP.PaymentDto("Porgas D' Ace", "Urb. El Corral Mz A CS 2")
        );

    }

    @Override
    public void loadPayments(LoadPaymentsCallback callback) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        callback.setPayments(payments);
    }
}
