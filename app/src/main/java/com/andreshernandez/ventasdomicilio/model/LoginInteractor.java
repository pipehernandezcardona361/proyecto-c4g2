package com.andreshernandez.ventasdomicilio.model;

import com.andreshernandez.ventasdomicilio.mvp.LoginMVP;

import java.util.HashMap;
import java.util.Map;


public class LoginInteractor implements LoginMVP.Model {

    private Map<String, String> users;

    public LoginInteractor() {
        users = new HashMap<>();
        users.put("Mihawk@vip.com", "12345678");
        users.put("Zoro@vip.com", "87654321");
    }

    @Override
    public void validateCredentials(String email, String password,
                                    LoginMVP.Model.ValidateCredentialsCallback callback) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if(users.get(email) != null
                && users.get(email).equals(password)){
            callback.onSuccess();
        } else {
            callback.onFailure();
        }
    }
}

